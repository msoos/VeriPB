from veripb.rules_multigoal import *
from veripb.timed_function import TimedFunction


@register_rule
class UpdateObjective(MultiGoalRule):
    Ids = ["obju"]

    @classmethod
    def parse(cls, words, context):
        if not context.objective:
            raise InvalidProof(
                "The rule 'obju' can only be used for optimization problems, but there was no objective specified in formula file.")
        cppWordIter = words.wordIter.getNative()
        objective_dict, objective_constant = context.ineqFactory.parseObjective(
            cppWordIter)

        maybe_separator = next(words, None)
        if maybe_separator is None:
            autoProveAll = True
        elif maybe_separator == cls.subProofBegin:
            autoProveAll = False
        elif maybe_separator == ";":
            autoProveAll = not cls.parseHasExplicitSubproof(words)
        else:
            ValueError("Unexpected word, expected 'begin'")

        return cls(context, objective_dict, objective_constant, autoProveAll)

    def __init__(self, context, objective_dict, objective_constant, autoProveAll):
        super().__init__(context)

        self.new_objective_terms = objective_dict
        self.new_objective_constant = objective_constant
        self.autoProveAll = autoProveAll

    def antecedentIDs(self):
        return "all"

    @TimedFunction.time("ObjectiveUpdate")
    def compute(self, antecedents, context):
        # proofgoal for f_new >= f_old
        terms = list()
        degree = context.objectiveConstant - self.new_objective_constant
        for lit, coeff in self.new_objective_terms.items():
            terms.append((coeff, lit))
        for lit, coeff in context.objective.items():
            terms.append((-coeff, lit))
        geq_ineq = context.ineqFactory.fromTerms(terms, degree)

        for idx in range(0, len(terms)):
            terms[idx] = (-terms[idx][0], terms[idx][1])
        leq_ineq = context.ineqFactory.fromTerms(terms, -degree)

        if context.proof:
            context.proof.print(
                "obju", " ".join(str(coeff) + " " + context.ineqFactory.int2lit(lit) for (lit, coeff) in self.new_objective_terms.items()), self.new_objective_constant, "; begin")

        if context.verifierSettings.trace:
            print("  ** proofgoal from new objective >= old objective **")
        geq_goal = SubGoal(geq_ineq)
        self.addSubgoal(geq_goal)
        if context.verifierSettings.trace:
            print("  ** proofgoal from old objective >= new objective **")
        leq_goal = SubGoal(leq_ineq)
        self.addSubgoal(leq_goal)

        if self.autoProveAll:
            self.autoProof(context, antecedents, True)
            context.objective = self.new_objective_terms
            context.objectiveConstant = self.new_objective_constant
            if context.proof:
                context.proof.print("end")
        else:  # There is an explicit subproof
            if context.only_core_subproof:
                raise NotImplementedError(
                    "Nesting of only core subproofs has not been implemented, yet. Only core subproofs are required for checked deletion and objective update.")
            context.only_core_subproof = True
            self.subContext.set_only_core_subproof = True
            self.addObjective(self.new_objective_terms,
                              self.new_objective_constant)

        return super().compute(antecedents, context)
