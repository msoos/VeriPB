from veripb import InvalidProof
from veripb.rules_register import register_rule, dom_friendly_rules, rules_to_dict
from veripb.parser import OPBParser, MaybeWordParser, ParseContext
from veripb.optimized.constraints import Substitution as CppSubstitution
from veripb.substitution import *

from veripb.timed_function import TimedFunction

from collections import deque

from veripb.printing import printRUPTrace


def objectiveToDict(formula):
    if (formula.hasObjective):
        coeffs = formula.objectiveCoeffs
        literals = formula.objectiveLits
        return {lit: coeff for (coeff, lit) in zip(coeffs, literals)}
    else:
        return None


class TemporaryAttach():
    def __init__(self, propEngine):
        self.propEngine = propEngine
        self.attached = []

    def attach(self, constraint):
        self.attached.append(self.propEngine.attach(
            constraint, 0))
        self.propEngine.moveToCore(self.attached[-1], 0)

    def detachAll(self):
        for c in self.attached:
            self.propEngine.detach(c, 0)

    def __enter__(self):
        return self

    def __exit__(self, exec_type, exec_value, exec_traceback):
        self.detachAll()


class Autoprover():
    # @TimedFunction.time("Autoprover::setup")
    def __init__(self, context, db, subgoals):
        self.context = context
        self.proof = context.proof
        self.subgoals = subgoals
        self.verbose = context.verifierSettings.trace
        self.traceFailed = context.verifierSettings.traceFailed
        self.context.propEngine.increaseNumVarsTo(
            context.ineqFactory.numVars())
        self.propEngine = context.propEngine
        self.db = db
        self.dbSubstituted = None
        self.dbSet = None
        self.assignment = None
        self.wasRUP = False
        self.triedRUP = False

    # @TimedFunction.time("Autoprover::propagate")
    def propagate(self, onlyCore=False):
        self.assignment = Substitution()
        self.assignment.addConstants(self.propEngine.propagatedLits(onlyCore))

    def getOutGoal(self, goalId, ineq):
        if isinstance(goalId, int):
            return ineq.getOutId(goalId)
        else:
            return goalId

    def inDB(self, nxtGoalId, nxtGoal, onlyCore):
        constraint = self.propEngine.find(nxtGoal, onlyCore)
        if constraint:
            if self.context.proof:
                self.context.proof.print("\t* autoproven: found in database")
                self.context.proof.print(
                    "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                cId = self.context.proof.nextId()
                self.context.proof.print(
                    "\t\tpol", cId, constraint.getOutId(constraint.minId), "+")
                cId = self.context.proof.nextId()
                self.context.proof.print("\tend", cId)

            if self.verbose:
                print("    automatically proved %s by finding constraint in database" % (
                    str(nxtGoalId)))
            return True
        return False

    # @TimedFunction.time("Autoprover::dbImplication")
    def dbImplication(self, nxtGoalId, nxtGoal, onlyCore):
        success = False
        if self.dbSubstituted is None:
            if not self.context.proof:
                asmnt = self.getPropagatedAssignment(onlyCore).get()
                self.dbSubstituted = [
                    (Id, ineq.copy().substitute(asmnt)) for Id, ineq in self.db]
            else:
                self.dbSubstituted = self.db

        for ineqId, ineq in self.dbSubstituted:
            if onlyCore:
                if not ineq.isCoreId(ineqId):
                    continue
            if ineq.implies(nxtGoal):
                success = True
                if self.context.proof:
                    self.context.proof.print(
                        "\t* autoproven: implied by database")
                    self.context.proof.print("\tproofgoal", nxtGoalId)
                    cId = self.context.proof.nextId()
                    self.context.proof.print(
                        "\t\tpol", cId, ineq.getOutId(), "+")
                    cId = self.context.proof.nextId()
                    self.context.proof.print("\tend", cId)
                break

        if success:
            if self.verbose:
                print("    automatically proved %s by implication from %i" %
                      (str(nxtGoalId), ineqId))
            return True
        return False

    # @TimedFunction.time("Autoprover::rupImplication")
    def rupImplication(self, nxtGoalId, nxtGoal, onlyCore):
        success = nxtGoal.rupCheck(
            self.propEngine, onlyCore, self.context.proof.getProofContext())
        if success:
            if self.context.proof:
                # If a proofgoal was proven by RUP
                if nxtGoalId:
                    self.context.proof.print("\t* autoproven: implied by RUP")
                    self.context.proof.print(
                        "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))

                self.context.proof.print(
                    "\t\tpol", self.context.proof.getBuffer())
                if nxtGoal != self.context.ineqFactory.fromTerms([], 1):
                    self.context.proof.nextId()

                # If a proofgoal was proven by RUP
                if nxtGoalId:
                    self.context.proof.print(
                        "\tend", self.context.proof.nextId())

            if self.verbose:
                if nxtGoalId is not None:
                    print("    automatically proved %s by RUP check." %
                          (str(nxtGoalId)))
                    # self.getPropagatedAssignment()
                else:
                    print(
                        "    automatically proved constraint to be added by RUP check.")
                    # self.getPropagatedAssignment()
            return True
        return False

    def getPropagatedAssignment(self, onlyCore=False):
        if self.assignment is None:
            self.propagate(onlyCore)

        return self.assignment

    @TimedFunction.time("Autoprover")
    def __call__(self, onlyCore):
        while self.subgoals:
            nxtGoalId, nxtGoal = self.subgoals.popleft()

            # Implication from the negated constraint and if proofgoal is in DB is already checked when creating proofgoals, hence it is already skipped.

            asRhs = nxtGoal.getAsRightHand()
            if asRhs is None:
                asLhs = nxtGoal.getAsLeftHand()

                with TemporaryAttach(self.propEngine) as temporary:
                    for c in asLhs:
                        if c is not None:
                            temporary.attach(c)
                            if self.context.proof:
                                # Set output proof constraint ID for constraints introduced by proofgoal
                                c.setOutId(
                                    c.minId, self.context.proof.nextId())

                    if self.rupImplication(nxtGoalId, self.context.ineqFactory.fromTerms([], 1), onlyCore):
                        continue

            else:
                nxtGoal = asRhs
                if nxtGoal.isTrivial():
                    if self.context.proof:
                        self.context.proof.print(
                            "\t* autoproven: trivial constraint")
                        self.context.proof.print(
                            "\tproofgoal", self.getOutGoal(nxtGoalId, nxtGoal))
                        cId = self.context.proof.nextId()
                        self.context.proof.print("\tend", cId)

                    if self.verbose:
                        print("    automatically proved %s, constraint is trivial." % (
                            str(nxtGoalId)))
                    continue

                if not self.triedRUP and self.rupImplication(None, self.context.ineqFactory.fromTerms([], 1), onlyCore):
                    self.wasRUP = True
                    break

                self.triedRUP = True

                # this is already checked when the effected constraints
                # are computed. However, due to caching it could be that
                # new constraints were added since then.
                if self.inDB(nxtGoalId, nxtGoal, onlyCore):
                    continue

                if self.rupImplication(nxtGoalId, nxtGoal, onlyCore):
                    continue

                # todo: optimisation should not be turned off when running self proof logging.
                if not self.context.proof:
                    # implication checks are stronger if we plug in propagated literals
                    cpNxtGoal = nxtGoal.copy()
                    nxtGoal = cpNxtGoal
                    # cpNxtGoal.copyId(nxtGoal)
                    asmnt = self.getPropagatedAssignment(onlyCore).get()
                    nxtGoalSubstituted = nxtGoal.substitute(asmnt)

                if self.dbImplication(nxtGoalId, nxtGoalSubstituted, onlyCore):
                    continue

            if self.traceFailed:
                printRUPTrace(self.context, onlyCore)
            raise InvalidProof(
                "Could not prove proof goal %s automatically. Please add an explicit proof for this proof goal. (using 'proofgoal %s')" % (str(nxtGoalId), str(nxtGoalId)))


def autoProof(context, db, subgoals, onlyCore=False):
    goals = deque(((nxtGoalId, nxtGoal) for nxtGoalId,
                  nxtGoal in subgoals.items() if not nxtGoal.isProven))
    subgoals.clear()
    context.rupWithoutSubgoals = False

    if not goals:
        return

    prover = Autoprover(context, db, goals)
    prover(onlyCore)

    if prover.wasRUP:
        context.autoRUPstreak = getattr(context, "autoRUPstreak", 0) + 1
    else:
        context.autoRUPstreak = 0

    context.rupWithoutSubgoals = prover.wasRUP
