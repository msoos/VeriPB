from veripb.optimized.constraints import Substitution as CppSubstitution
import itertools
from veripb import InvalidProof


class Substitution:
    def __init__(self):
        self.constants = []
        self.substitutions = []
        self.isSorted = True

    def isEmpty(self):
        if self.constants == [] and self.substitutions == []:
            return True
        else:
            return False

    def mapAll(self, variables, substitutes):
        for variable, substitute in zip(variables, substitutes):
            self.map(variable, substitute)

    def addConstants(self, constants):
        self.isSorted = False
        self.constants.extend(constants)

    def map(self, variable, substitute):
        self.isSorted = False

        if variable < 0:
            raise ValueError("Substitution should only map variables.")

        if substitute is False:
            self.constants.append(-variable)
        elif substitute is True:
            self.constants.append(variable)
        else:
            self.substitutions.append((variable, substitute))

    def get(self):
        if len(self.substitutions) > 0:
            frm, to = zip(*self.substitutions)
        else:
            frm = []
            to = []

        return CppSubstitution(self.constants, frm, to)

    def asDict(self):
        res = dict()
        for var, value in self.substitutions:
            res[var] = value
            res[-var] = -value

        for lit in self.constants:
            res[lit] = True
            res[-lit] = False

        return res

    def contains(self, var):
        if var in self.constants:
            return True
        for frm, _ in self.substitutions:
            if frm == var:
                return True
        return False

    @classmethod
    def fromDict(cls, sub):
        res = Substitution()
        for key, value in sub.items():
            res.map(key, value)
        res.sort()
        return res

    def sort(self):
        if not self.isSorted:
            self.substitutions.sort(key=lambda x: x[0])
            self.constants.sort()
            self.isSorted = True

    @classmethod
    def parse(cls, words, ineqFactory, forbidden=[]):
        result = cls()

        try:
            nxt = next(words)
        except StopIteration:
            return result

        while nxt != ";":
            frm = ineqFactory.lit2int(nxt)
            if frm < 0:
                raise ValueError("Substitution should only"
                                 "map variables, not negated literals.")
            if frm in forbidden:
                raise InvalidProof("Substitution contains forbidden variable.")
            if result.contains(frm):
                raise InvalidProof(
                    "Substitution already contains %s as variable that should be substituted (left-hand side of the substitution). Cannot substitute one variable to two values/literals in one substitution." % ineqFactory.int2lit(frm))

            try:
                nxt = next(words)
                if nxt == "→" or nxt == "->":
                    nxt = next(words)
            except StopIteration:
                raise ValueError("Substitution is missing"
                                 "a value for the last variable.")

            if nxt == "0":
                to = False
            elif nxt == "1":
                to = True
            else:
                to = ineqFactory.lit2int(nxt)

            result.map(frm, to)

            try:
                nxt = next(words)

                if nxt == ",":
                    nxt = next(words)
            except StopIteration:
                break

        return result

    def __eq__(self, other):
        self.sort()
        return self.constants == other.constants \
            and self.substitutions == other.substitutions

    def __hash__(self):
        self.sort()
        return hash((tuple(self.constants), tuple(self.substitutions)))

    def toString(self, ineqFactory):
        constants = ((ineqFactory.num2Name(abs(lit)), "1" if lit >
                     0 else "0") for lit in self.constants)
        substitutions = ((ineqFactory.num2Name(var), ineqFactory.int2lit(substitute))
                         for var, substitute in self.substitutions)

        it = itertools.chain(constants, substitutions)

        return " ".join(("%s -> %s" % (var, subst) for var, subst in it))
