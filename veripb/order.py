from collections import defaultdict
from veripb import InvalidProof
from veripb.substitution import *
from veripb.subgoal import *


# @TimedFunction.time("computeEffected")
def computeEffected(context, substitution, only_core_subproof, onlyCoreProofgoals=False):
    # leave one id for the negated constraint
    nextId = context.proof.numConstraints + 1
    proofContext = context.proof.getProofContext(nextId)
    return context.propEngine.computeEffected(substitution, proofContext, only_core_subproof, onlyCoreProofgoals, proofContext != None)


class TransitivityInfo:
    def __init__(self):
        self.fresh_right = []
        self.fresh_aux_1 = []
        self.fresh_aux_2 = []
        self.isProven = False


class GoalCache:
    def __init__(self):
        self.goals = None


class Order:
    def __init__(self, name=""):
        self.name = name
        self.definition = []
        self.auxDefinition = None
        self.leftVars = []
        self.rightVars = []
        self.auxVars = []

        # variables the order is actually defined over, will be set
        # when order is loaded.
        self.vars = None

        self.transitivity = TransitivityInfo()
        self.reflexivityProven = False

        self.goalCache = defaultdict(GoalCache)

    def reflexivityCheck(self, context):
        context.proof.print("\treflexivity")
        context.proof.print("\t\tproof")

        substitution = Substitution()
        substitution.mapAll(self.rightVars, self.leftVars)
        sub = substitution.get()
        counter = 1
        for ineq in self.definition:
            ineq = ineq.copy()
            ineq.substitute(sub)
            if not ineq.isTrivial():
                return False

            context.proof.print("\t\t\tproofgoal #" + str(counter))
            context.proof.print("\t\t\tend", context.proof.nextId())
            counter += 1

        context.proof.print("\t\tend")
        context.proof.print("\tend")
        return True

    def check(self, context):
        if not self.transitivity.isProven:
            raise InvalidProof("Proof did not show transitivity of order.")
        if not self.reflexivityProven:
            if not self.reflexivityCheck(context):
                raise InvalidProof(
                    "Reflexivity of order is not trivial. An explicit proof for reflexivity has to be provided (use 'reflexivity' keyword).")

    def reset(self):
        self.goalCache = defaultdict(GoalCache)
        self.vars = None

    def getOrderCondition(self, witnessDict):
        res = []
        zippedVars = zip(
            self.leftVars,
            self.rightVars,
            self.vars)

        substitution = Substitution()
        for leftVar, rightVar, var in zippedVars:
            try:
                substitution.map(leftVar, witnessDict[var])
            except KeyError:
                substitution.map(leftVar, var)
            substitution.map(rightVar, var)

        for aux in self.auxVars:
            try:
                substitution.map(aux, witnessDict[aux])
            except KeyError:
                pass

        # # todo: we need to check that the auxVars are still fresh
        sub = substitution.get()

        # for ineq in self.order.auxDefinition:
        #     ineq = self.constraint.copy()
        #     ineq.substitute(sub)
        #     self.addAvailable(ineq)

        for ineq in self.definition:
            ineq = ineq.copy()
            ineq.substitute(sub)
            res.append(ineq)
        return res

    def getOrderConditionFlipped(self, witnessDict):
        res = []
        zippedVars = zip(
            self.leftVars,
            self.rightVars,
            self.vars)

        substitution = Substitution()
        for leftVar, rightVar, var in zippedVars:
            try:
                substitution.map(rightVar, witnessDict[var])
            except KeyError:
                substitution.map(rightVar, var)
            substitution.map(leftVar, var)

        # todo: aux vars????

        sub = substitution.get()

        for ineq in self.definition:
            ineq = ineq.copy()
            ineq.substitute(sub)
            res.append(ineq)
        return res

    def getCachedGoals(self, context, witness):
        cache = self.goalCache[witness]
        if cache.goals is None:
            cache.goals = [SubGoal(ineq)
                           for ineq in computeEffected(context, witness.get(), context.only_core_subproof, True)]

            witnessDict = witness.asDict()

            cache.goals.extend((SubGoal(ineq)
                                for ineq in self.getOrderCondition(witnessDict)))

            cache.goals.append(NegatedSubGoals(
                self.getOrderConditionFlipped(witnessDict)))

            obj = objectiveCondition(context, witness.asDict())
            if obj is not None:
                cache.goals.append(SubGoal(obj))
        else:
            for goal in cache.goals:
                goal.isProven = False
        return cache.goals


class OrderContext:
    @classmethod
    def setup(cls, context):
        # we access this directly in the verifier on deletion,
        # remember to change there if changed here.
        try:
            return context.orderContext
        except AttributeError:
            context.orderContext = cls(context)
            return context.orderContext

    def __init__(self, context):
        self.orders = dict()

        self.emptyOrder = Order()
        self.orders[self.emptyOrder.name] = \
            self.emptyOrder
        self.activeOrder = None
        self.resetToEmptyOrder()

        self.activeDefinition = None

    def resetToEmptyOrder(self):
        self.activateOrder(self.emptyOrder, [])

    def activateOrder(self, order, lits):
        if self.activeOrder is not None:
            self.activeOrder.reset()

        order.vars = lits
        order.varsSet = set(lits)
        self.activeOrder = order

    def newOrder(self, name):
        newOrder = Order(name)
        if self.activeDefinition is not None:
            raise ValueError("Tried to create new order while previous"
                             "order definition was not finished.")

        self.activeDefinition = newOrder

    def qedNewOrder(self, context):
        newOrder = self.activeDefinition
        newOrder.check(context)
        self.orders[newOrder.name] = newOrder
        self.activeDefinition = None

        return []


def objectiveCondition(context, witnessDict):
    if getattr(context, "objective", None) is None:
        return None

    # obj(x\omega) \leq obj(x)
    terms = []
    degree = 0

    for lit, coeff in context.objective.items():
        try:
            lit2 = witnessDict[lit]

            terms.append((coeff, lit))

            if lit2 is True:
                degree += coeff
            elif lit2 is not False:
                terms.append((-coeff, lit2))

        except KeyError:
            # note that literals that are not remaped will disapear
            # anyway, so no reason to add them
            pass

    return context.ineqFactory.fromTerms(terms, degree)
