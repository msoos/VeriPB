from string import Template


def print_pretty(useColor, *args, **kwargs):
    if useColor:
        colors = {
            "cid": "\u001b[36m",
            "obj": "\u001b[32;1m",
            "reset": "\u001b[0m",
            "ineq": "\u001b[34;1m",
            "lit": "\u001b[95;1m"
        }
    else:
        colors = {
            "cid": "",
            "obj": "",
            "reset": "",
            "ineq": "",
            "lit": ""
        }

    args = [Template(arg).substitute(**colors) for arg in args]
    print(*args, **kwargs)


def printRUPTrace(context, onlyCore=False):
    print("RUP check failed! The RUP check has the following trace:")
    print("  propagations in format '<assignment> (<reason constraint>)':")
    context.propEngine.printPropagationTrace(
        context.ineqFactory.varNameMgr, context.verifierSettings.useColor, onlyCore)
