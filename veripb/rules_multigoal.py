from veripb import InvalidProof
from veripb.rules import EmptyRule
from veripb.rules import ReversePolishNotation, IsContradiction, ContradictionCheckFailed
from veripb.rules_register import register_rule, dom_friendly_rules, rules_to_dict
from veripb.parser import OPBParser, MaybeWordParser, ParseContext
from veripb.subgoal import *

from veripb import verifier

from veripb.exceptions import ParseError

from veripb.timed_function import TimedFunction

from collections import deque

from veripb.autoproving import autoProof, TemporaryAttach

from veripb.optimized.constraints import maxId as getMaxConstraintId
constraintMaxId = getMaxConstraintId()


class SubContextInfo():
    def __init__(self):
        self.toDelete = []
        self.toAdd = []
        self.previousRules = None
        self.callbacks = []
        self.subgoals = dict()
        self.witness = None
        self.objective_terms = None
        self.objective_constant = None
        self.set_only_core_subproof = False
        self.add_result_to_core = False

    def addToDelete(self, ineqs):
        self.toDelete.extend(ineqs)

    def makeSubgoal(self, constraint):
        if self.witness is None:
            return None
        else:
            c = constraint.copy()
            c.substitute(self.witness)
            c.negated()
            return c


class SubContext():
    @classmethod
    def setup(cls, context):
        try:
            return context.subContexts
        except AttributeError:
            context.subContexts = cls(context)
            return context.subContexts

    def __init__(self, context):
        self.infos = []
        self.context = context

        def f(ineqs, context): return self.addToDelete(ineqs)
        context.addIneqListener.append(f)

    def __bool__(self):
        return bool(self.infos)

    def addToDelete(self, ineqs):
        if len(self.infos) > 0:
            self.infos[-1].addToDelete(ineqs)

    def push(self):
        newSubContext = SubContextInfo()
        self.infos.append(newSubContext)
        return self.infos[-1]

    def pop(self, checkSubgoals=True):
        oldContext = self.infos.pop()
        for callback in oldContext.callbacks:
            callback(self.context, oldContext)

        if checkSubgoals and len(oldContext.subgoals) > 0:
            for Id, subgoal in oldContext.subgoals.items():
                raise InvalidProof("Open subgoal not proven: %s: %s" % (
                    str(Id), subgoal.toString(self.context.ineqFactory)))

        return oldContext

    def getCurrent(self):
        return self.infos[-1]


class EndOfProof(EmptyRule):
    Ids = ["qed", "end"]

    @classmethod
    def parse(cls, line, context):
        subContexts = SubContext.setup(context)
        if not subContexts:
            raise ParseError("No subcontext to end here.")

        subContext = subContexts.getCurrent()
        with MaybeWordParser(line) as words:
            which = list(map(str, words))

            if context.major == 1:
                conflictingConstraintId = None
            if context.major >= 2:
                # TODO: enforce that end has to have constraint ID if required
                if (len(which) != 1):
                    conflictingConstraintId = None
                    # raise ValueError(
                    #     "Subproof should end with 'end <conflicting_constraint_id>'.")
                else:
                    try:
                        conflictingConstraintId = int(which[0])
                    except ValueError:
                        raise ValueError(
                            "Subproof has to end with 'end <conflicting_constraint_id>'. However, <conflicting_constraint_id> was not an integer, but %s." % which)

        return cls(subContext, conflictingConstraintId)

    def __init__(self, subcontext, conflictingConstraintId):
        self.subcontext = subcontext
        if conflictingConstraintId:
            self.conflictingConstraintId = conflictingConstraintId
        else:
            self.conflictingConstraintId = None

    def deleteConstraints(self):
        return self.subcontext.toDelete

    def compute(self, antecedents, context):
        if self.conflictingConstraintId:
            antecedent = next(antecedents)
            if context.only_core_subproof and not antecedent[0].isCoreId(antecedent[1]):
                raise InvalidProof(
                    "Ended subproof that can only use core constraints with derived constraint ID %d." % antecedent[1])

            if not antecedent[0].isContradiction():
                raise ContradictionCheckFailed()
            else:
                context.containsContradiction = True

        if self.subcontext.subgoals:
            if getattr(context, "containsContradiction", False):
                self.subcontext.subgoals.clear()
            else:
                autoProof(context, antecedents,
                          self.subcontext.subgoals, context.only_core_subproof)

        subContexts = SubContext.setup(context)
        poped = subContexts.pop()

        if getattr(context, "containsContradiction", False):
            context.containsContradiction = False
            # todo: instead of setting contains contradiction to false
            # we should restore the previous state.

        assert (self.subcontext == poped)

        # Update objective an objective update is stored for this subproof
        if self.subcontext.objective_terms:
            context.objective = self.subcontext.objective_terms
            context.objectiveConstant = self.subcontext.objective_constant

        if self.subcontext.set_only_core_subproof:
            context.only_core_subproof = False

        return self.subcontext.toAdd

    def antecedentIDs(self):
        if self.conflictingConstraintId:
            return [self.conflictingConstraintId]
        else:
            return "all"

    def allowedRules(self, context, currentRules):
        if self.subcontext.previousRules is not None:
            return self.subcontext.previousRules
        else:
            return currentRules

    def numConstraints(self):
        return len(self.subcontext.toAdd)

    def isAddToCore(self):
        return self.subcontext.add_result_to_core

    def justify(self, antecedents, derived, context):
        if self.conflictingConstraintId:
            antecedent = list(antecedents)[0]
            context.proof.print("end", antecedent[0].getOutId(antecedent[1]))
        else:
            # Check if autoproving was done to proof remaining subgoals
            if getattr(context, "rupWithoutSubgoals", False):
                context.proof.print("end", context.proof.nextId())
            # Or only subgoal by subgoal
            else:
                context.proof.print("end")

        for constraint, idx in derived:
            cId = context.proof.nextId()
            if constraint:
                constraint.setOutId(idx, cId)


class SubProof(EmptyRule):
    Ids = ["proofgoal"]

    # todo enforce only one def

    @classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subContext = subcontexts.getCurrent()

        with MaybeWordParser(line) as words:
            myGoal = next(words)
            if myGoal[0] != "#":
                myGoal = int(myGoal)
            else:
                if not subContext.subgoals:
                    raise ValueError("No proofgoals left to proof.")

                if myGoal not in subContext.subgoals:
                    raise ValueError("Invalid proofgoal.")

        return cls(subContext.subgoals, myGoal)

    def __init__(self, subgoals, myGoal):
        self.subRules = dom_friendly_rules() + [EndOfProof]
        self.myGoal = myGoal
        if isinstance(myGoal, int):
            self._antecedentIDs = [myGoal]
        else:
            self._antecedentIDs = []
        self.subgoals = subgoals

    def compute(self, antecedents, context):
        subContexts = SubContext.setup(context)
        parentContext = subContexts.getCurrent()

        self.subContext = subContexts.push()

        def f(context, subContext): return self.check(context)
        self.subContext.callbacks.append(f)

        self.subgoal = self.subgoals.get(self.myGoal, None)
        if self.subgoal is None:
            dbConstraint, _idx = next(antecedents)
            return [parentContext.makeSubgoal(dbConstraint)]
        else:
            del self.subgoals[self.myGoal]
            return self.subgoal.getAsLeftHand()

    def antecedentIDs(self):
        return self._antecedentIDs

    def numConstraints(self):
        return 1

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def check(self, context):
        if not getattr(context, "containsContradiction", False):
            raise InvalidProof("Sub proof did not show contradiction.")

        context.containsContradiction = False

    def justify(self, antecedents, derived, context):
        context.proof.print("* prove provided by user")
        if isinstance(self.myGoal, str):
            context.proof.print("proofgoal", self.myGoal)
        else:
            context.proof.print(
                "proofgoal", self.subgoal.constraint.getOutId(self.subgoal.constraint.minId))

        for constraint, idx in derived:
            # todo what if constraint added through proofgoal is duplicate?
            if constraint is not None:
                constraint.setOutId(idx, context.proof.nextId())


class MultiGoalRule(EmptyRule):
    subRules = [EndOfProof, SubProof]
    subProofBegin = "begin"

    @classmethod
    def parseHasExplicitSubproof(cls, words):
        try:
            nxt = next(words)
            if nxt != cls.subProofBegin:
                raise ValueError("Unexpected word, expected 'begin'")
            return True
        except StopIteration:
            return False

    def __init__(self, context):
        self.subContexts = SubContext.setup(context)
        self.subContext = self.subContexts.push()
        self.displayGoals = context.verifierSettings.trace
        self.constraints = []
        self.ineqFactory = context.ineqFactory
        self.nextId = 1
        self.autoProoved = False

    def addSubgoal(self, goal, Id=None):
        if Id is None or Id == constraintMaxId:
            # the goal does not relate to an existing constraint
            Id = "#%i" % (self.nextId)
            self.nextId += 1

        assert (Id not in self.subContext.subgoals)
        self.subContext.subgoals[Id] = goal
        if self.displayGoals:
            ineqStr = goal.toString(self.ineqFactory)
            sId = Id
            if isinstance(sId, int):
                sId = "%03i" % (sId)
            print("  proofgoal %s: %s" % (sId, ineqStr))

        return Id

    def addAvailable(self, ineq):
        """add constraint available in sub proof"""
        self.constraints.append(ineq)

    def autoProof(self, context, db, onlyCore=False):
        self.autoProoved = True

        if self.subContext.subgoals:
            added = list()

            with TemporaryAttach(context.propEngine) as temporary:
                for c in self.constraints:
                    if c is not None:
                        # TODO investigate why the outId is lost when calling `attach`
                        if context.proof:
                            outId = c.getOutId(c.minId)
                            if outId == 0:
                                raise ValueError
                        temporary.attach(c)
                        if context.proof:
                            c.setOutId(c.minId, outId)

                autoProof(context, db, self.subContext.subgoals, onlyCore)

        self.constraints = self.subContext.toAdd
        self.subContexts.pop()

    def addIntroduced(self, ineq):
        """add constraint introduced after all subgoals are proven"""
        self.subContext.toAdd.append(ineq)

    def addObjective(self, objective_terms, objective_constant):
        """Add objective to be updated after all subgoals are proven"""
        self.subContext.objective_terms = objective_terms
        self.subContext.objective_constant = objective_constant

    def compute(self, antecedents, context=None):
        return self.constraints

    def numConstraints(self):
        return len(self.constraints)

    def allowedRules(self, context, currentRules):
        if not self.autoProoved:
            self.subContext.previousRules = currentRules
            return rules_to_dict(self.subRules)
        else:
            return currentRules
