import logging
from collections import defaultdict
from collections import deque
from veripb.autoproving import *
from veripb.rules_multigoal import *
from veripb.timed_function import TimedFunction
from veripb import verifier
from veripb import InvalidProof
from veripb.rules import Rule, EmptyRule, register_rule
from veripb.rules import ReversePolishNotation, IsContradiction, ConstraintEquals
from veripb.rules_register import register_rule, dom_friendly_rules, rules_to_dict
from veripb.parser import OPBParser, MaybeWordParser, ParseContext
from veripb.prooflogging import Proof
from veripb.order import *
from veripb.optimized.constraints import maxId as getMaxConstraintId
constraintMaxId = getMaxConstraintId()


@register_rule
class LoadOrder(EmptyRule):
    Ids = ["load_order"]

    # todo: allow negated literals

    @classmethod
    def parse(cls, line, context):
        orderContext = OrderContext.setup(context)

        with MaybeWordParser(line) as words:
            cls.lits = []
            cls.name = ""
            try:
                cls.name = next(words)
            except StopIteration:
                orderContext.resetToEmptyOrder()
                return cls()

            for nxt in words:
                lit = context.ineqFactory.lit2int(nxt)
                cls.lits.append(lit)

            try:
                order = orderContext.orders[cls.name]
            except KeyError:
                raise ValueError("Unknown order '%s'." % (cls.name))

        if len(cls.lits) != len(order.leftVars):
            raise ValueError(
                "Order does not specify right number of"
                "variables. Expected: %i, Got: %i"
                % (len(order.leftVars), len(cls.lits)))

        orderContext.activateOrder(order, cls.lits)

        return cls()

    def compute(self, antecedents, context):
        orderContext = OrderContext.setup(context)
        # Check if the new order is not the empty order
        if self.name != "":
            context.propEngine.moveAllToCore()
        return []

    def justify(self, antecedents, derived, context):
        context.proof.print("load_order", self.name, " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class OrderVarsBase(EmptyRule):
    @classmethod
    def addLits(cls, order, lits):
        pass

    @classmethod
    def parse(cls, line, context):
        lits = []
        with MaybeWordParser(line) as words:
            for nxt in words:
                lits.append(context.ineqFactory.lit2int(nxt))

        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())

        order = OrderContext.setup(context)
        cls.addLits(order.activeDefinition, lits)
        cls.lits = lits
        return cls()


class LeftVars(OrderVarsBase):
    Ids = ["left"]

    @classmethod
    def addLits(cls, order, lits):
        order.leftVars.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\tleft", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class RightVars(OrderVarsBase):
    Ids = ["right"]

    @ classmethod
    def addLits(cls, order, lits):
        order.rightVars.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\tright", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class AuxVars(OrderVarsBase):
    Ids = ["aux"]

    @ classmethod
    def addLits(cls, order, lits):
        order.auxVars.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\taux", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class OrderVars(EmptyRule):
    Ids = ["vars"]
    subRules = [LeftVars, RightVars, AuxVars, EndOfProof]

    @ classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subcontext = subcontexts.push()

        return cls(subcontext)

    def __init__(self, subContext):
        self.subContext = subContext
        def f(context, subContext): return self.check(context)
        subContext.callbacks.append(f)

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def check(self, context):
        order = OrderContext.setup(context).activeDefinition

        leftSet = set(order.leftVars)
        rightSet = set(order.rightVars)

        sizes = {len(leftSet), len(rightSet), len(
            order.leftVars), len(order.rightVars)}
        if len(sizes) != 1:
            raise InvalidProof(
                "Number of variables specified in left and right did not match.")
        if not leftSet.isdisjoint(rightSet):
            raise InvalidProof(
                "Variables specified in left and right are not disjoint.")

    def justify(self, antecedents, derived, context):
        context.proof.print("\tvars")


class OrderDefinition(EmptyRule):
    Ids = [""]

    @ classmethod
    def parse(cls, line, context):
        lits = []
        with MaybeWordParser(line) as words:
            parser = OPBParser(
                ineqFactory=context.ineqFactory,
                allowEq=True)
            cls.ineqs = parser.parseConstraint(words)

        orderContext = OrderContext.setup(context)
        order = orderContext.activeDefinition
        order.definition.extend(cls.ineqs)

        return cls()

    def justify(self, antecedents, derived, context):
        context.proof.print(
            "\t\t" + context.ineqFactory.toString(self.ineqs[0]), ";")


class OrderDefinitions(EmptyRule):
    Ids = ["def"]
    subRules = [EndOfProof, OrderDefinition]

    # todo enforce only one def

    @ classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subcontext = subcontexts.push()

        return cls(subcontext)

    def __init__(self, subContext):
        self.subContext = subContext

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def justify(self, antecedents, derived, context):
        context.proof.print("\tdef")


class ReflexivityProof(MultiGoalRule):
    Ids = ["proof"]

    @ classmethod
    def parse(cls, line, context):
        orderContext = OrderContext.setup(context)
        order = orderContext.activeDefinition

        # This rule will fail if the proof is not correct so lets
        # already mark it proven here.
        order.reflexivityProven = True

        return cls(context, order)

    def __init__(self, context, order):
        super().__init__(context)

        self.subRules = dom_friendly_rules() + [EndOfProof, SubProof]

        substitution = Substitution()
        substitution.mapAll(order.rightVars, order.leftVars)
        sub = substitution.get()
        for ineq in order.definition:
            ineq = ineq.copy()
            ineq.substitute(sub)
            self.addSubgoal(SubGoal(ineq))

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\tproof")

        # Add constraintIDs to temporarily added constraints
        for constraint, idx in derived:
            cId = context.proof.nextId()
            constraint.setOutId(idx, cId)


class Reflexivity(EmptyRule):
    Ids = ["reflexivity"]
    subRules = [EndOfProof, ReflexivityProof]

    @ classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subcontext = subcontexts.push()

        orderContext = OrderContext.setup(context)
        order = orderContext.activeDefinition

        if not order.transitivity.isProven:
            raise InvalidProof(
                "The transitivity proof of an order has to come before the reflexivity proof.")

        return cls(subcontext, order)

    def __init__(self, subContext, order):
        self.subContext = subContext
        self.order = order

        def f(context, subContext): return self.check(context)
        subContext.callbacks.append(f)

    def check(self, context):
        if not self.order.reflexivityProven:
            raise InvalidProof("Reflexivity proof is missing.")

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def justify(self, antecedents, derived, context):
        context.proof.print("\treflexivity")


class TransitivityFreshRight(OrderVarsBase):
    Ids = ["fresh_right"]

    @ classmethod
    def addLits(cls, order, lits):
        order.transitivity.fresh_right.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\t\tfresh_right", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class TransitivityFreshAux1(OrderVarsBase):
    Ids = ["fresh_aux1"]

    @ classmethod
    def addLits(cls, order, lits):
        order.transitivity.fresh_aux_1.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\t\tfresh_aux1", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class TransitivityFreshAux2(OrderVarsBase):
    Ids = ["fresh_aux2"]

    @ classmethod
    def addLits(cls, order, lits):
        order.transitivity.fresh_aux_2.extend(lits)

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\t\tfresh_aux2", " ".join(
            [context.ineqFactory.int2lit(lit) for lit in self.lits]))


class TransitivityVars(EmptyRule):
    Ids = ["vars"]
    subRules = [TransitivityFreshRight, TransitivityFreshAux1,
                TransitivityFreshAux2, EndOfProof]

    @ classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subcontext = subcontexts.push()

        return cls(subcontext)

    def __init__(self, subContext):
        self.subContext = subContext
        def f(context, subContext): return self.check(context)
        subContext.callbacks.append(f)

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def check(self, context):
        order = OrderContext.setup(context).activeDefinition

        leftSet = set(order.leftVars)
        rightSet = set(order.rightVars)
        freshSet = set(order.transitivity.fresh_right)

        sizes = {len(leftSet), len(rightSet), len(order.leftVars),
                 len(order.rightVars), len(freshSet), len(order.transitivity.fresh_right)}
        if len(sizes) != 1:
            raise InvalidProof(
                "Number of variables specified in left and right did not match.")
        if not leftSet.isdisjoint(freshSet) or not rightSet.isdisjoint(freshSet):
            raise InvalidProof(
                "Variables specified in left and right are not disjoint.")

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\tvars")


class TransitivityProof(MultiGoalRule):
    Ids = ["proof"]

    @ classmethod
    def parse(cls, line, context):
        orderContext = OrderContext.setup(context)
        order = orderContext.activeDefinition

        # This rule will fail if the proof is not correct so lets
        # already mark it proven here.
        order.transitivity.isProven = True

        return cls(context, order)

    def __init__(self, context, order):
        super().__init__(context)

        self.subRules = dom_friendly_rules() + [EndOfProof, SubProof]

        for ineq in order.definition:
            ineq = ineq.copy()
            self.addAvailable(ineq)

        substitution = Substitution()
        substitution.mapAll(order.leftVars, order.rightVars)
        substitution.mapAll(order.rightVars, order.transitivity.fresh_right)

        sub = substitution.get()
        for ineq in order.definition:
            ineq = ineq.copy()

            ineq.substitute(sub)
            self.addAvailable(ineq)

        substitution = Substitution()
        substitution.mapAll(order.rightVars, order.transitivity.fresh_right)

        sub = substitution.get()
        for ineq in order.definition:
            ineq = ineq.copy()

            ineq.substitute(sub)

            self.addSubgoal(SubGoal(ineq))

    def justify(self, antecedents, derived, context):
        context.proof.print("\t\tproof")

        # Add constraintIDs to temporarily added constraints
        for constraint, idx in derived:
            cId = context.proof.nextId()
            constraint.setOutId(idx, cId)


class Transitivity(EmptyRule):
    Ids = ["transitivity"]
    subRules = [EndOfProof, TransitivityVars, TransitivityProof]

    @ classmethod
    def parse(cls, line, context):
        subcontexts = SubContext.setup(context)
        subcontext = subcontexts.push()

        orderContext = OrderContext.setup(context)
        order = orderContext.activeDefinition

        return cls(subcontext, order)

    def __init__(self, subContext, order):
        self.subContext = subContext
        self.order = order

        def f(context, subContext): return self.check(context)
        subContext.callbacks.append(f)

    def check(self, context):
        if not self.order.transitivity.isProven:
            raise InvalidProof("Transitivity proof is missing.")

    def allowedRules(self, context, currentRules):
        self.subContext.previousRules = currentRules
        return rules_to_dict(self.subRules)

    def justify(self, antecedents, derived, context):
        context.proof.print("\ttransitivity")


class StopSubVerifier(Exception):
    pass


class SubVerifierEnd(EmptyRule):
    Ids = ["qed", "end"]

    def __init__(self, oldParseContext):
        self.oldParseContext = oldParseContext

    def compute(self, antecedents, context):
        orders = OrderContext.setup(context)
        orders.qedNewOrder(context)

        # `end` must be written to output proof before subverifier is stopped
        if context.proof:
            context.proof.print("end")

        if context.verifierSettings.trace:
            print("=== end trace ===")
            print()

        raise StopSubVerifier()

    def newParseContext(self, parseContext):
        return self.oldParseContext


class SubVerifierEndRule():
    def __init__(self, oldParseContext):
        self.oldParseContext = oldParseContext

    def parse(self, line, context):
        return SubVerifierEnd(self.oldParseContext)


class SubVerifier(EmptyRule):
    def compute(self, antecedents, context):
        # context for sub verifier
        svContext = verifier.Context()
        svContext.newIneqFactory = context.newIneqFactory
        svContext.ineqFactory = svContext.newIneqFactory()
        svContext.newPropEngine = context.newPropEngine
        svContext.propEngine = svContext.newPropEngine()
        svContext.ruleCount = getattr(context, "ruleCount", 0)
        svContext.proof = Proof(
            context.proof.file, context.proof.tmp_buffer, [], context.proof.getProofContext().getVariableNameManager(), True)
        svContext.major = context.major
        svContext.minor = context.minor

        self._newParseContext = ParseContext(svContext)

        self.onEnterSubVerifier(context, svContext)

        try:
            verify = verifier.Verifier(
                context=svContext,
                settings=context.verifierSettings)
            verify(context.rules)
        except StopSubVerifier:
            self.exitSubVerifier(context, svContext)
            svContext.propEngine = None
            svContext.ineqFactory = None
        else:
            raise InvalidProof("Subproof not finished.")

        return []

    def exitSubVerifier(self, context, subVerifierContext):
        context.usesAssumptions = getattr(
            subVerifierContext, "usesAssumptions", False)
        self.onExitSubVerifier(context, subVerifierContext)

    def onEnterSubVerifier(self, context, subVerifierContext):
        pass

    def onExitSubVerifier(self, context, subVerifierContext):
        pass

    def newParseContext(self, parseContext):
        self._newParseContext.rules = \
            self.allowedRules(
                parseContext.context,
                parseContext.rules)

        rule = SubVerifierEndRule(parseContext)
        for Id in SubVerifierEnd.Ids:
            self._newParseContext.rules[Id] = rule

        self._newParseContext.context.forward_comments_to_output_proof = False

        return self._newParseContext


@ register_rule
class StrictOrder(SubVerifier):
    Ids = ["pre_order", "def_order"]
    subRules = [OrderVars, OrderDefinitions, Transitivity, Reflexivity]

    @ classmethod
    def parse(cls, line, context):
        with MaybeWordParser(line) as words:
            name = next(words)

        return cls(name)

    def __init__(self, name):
        self.name = name

    def onEnterSubVerifier(self, context, subContext):
        orders = OrderContext.setup(subContext)
        orders.newOrder(self.name)

        # write that we define and prove a pre_order before changing to subverifier, hence justify cannot be used.
        if context.proof:
            context.proof.print("pre_order", self.name)

    def onExitSubVerifier(self, context, subContext):
        orderSubContext = OrderContext.setup(subContext)
        orderContext = OrderContext.setup(context)
        orderContext.orders[self.name] = orderSubContext.orders[self.name]

    def allowedRules(self, context, currentRules):
        return rules_to_dict(self.subRules)


class Stats:
    def __init__(self):
        self.numGoalCandidates = 0
        self.numSubgoals = 0

    def print_stats(self):
        for attr in dir(self):
            value = getattr(self, attr)
            if not callable(value) and not attr.startswith("__"):
                print("c statistic: %s: %s" % (attr, str(value)))


stats = Stats()


@ register_rule
class AddRedundant(MultiGoalRule):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["red"]

    @ classmethod
    def parse(cls, words, context):
        cppWordIter = words.wordIter.getNative()
        ineq = context.ineqFactory.parse(cppWordIter, allowMultiple=False)

        # parser = OPBParser(
        #     ineqFactory = context.ineqFactory,
        #     allowEq = False)
        # ineq = parser.parseConstraint(words)

        context.canLoadFormula = False

        substitution = Substitution.parse(
            words=words,
            ineqFactory=context.ineqFactory)

        context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())

        autoProveAll = not cls.parseHasExplicitSubproof(words)

        return cls(context, ineq, substitution, autoProveAll)

    def __init__(self, context, constraint, witness, autoProveAll):
        super().__init__(context)

        self.constraint = constraint
        self.witness = witness

        self.autoProveAll = autoProveAll
        self.strengthening_to_core = context.strengthening_to_core

        self.addIntroduced(constraint)

    def antecedentIDs(self):
        return "all"

    def makeGoal(self, ineq, negated, goalId=None):
        goal = SubGoal(ineq)
        if negated.implies(ineq):
            goal.isProven = True
        else:
            stats.numSubgoals += 1
        goalId = self.addSubgoal(goal, goalId)

        if self.proof and goal.isProven:
            self.proof.print(
                "\t* autoproven: goal is implied by negated constraint")
            if isinstance(goalId, int):
                self.proof.print("\tproofgoal", ineq.getOutId(goalId))
            else:
                self.proof.print("\tproofgoal", goalId)
            cId = self.proof.nextId()

            self.proof.print("\t\tpol", cId, self.outNegRedId, "+")
            cId = self.proof.nextId()
            self.proof.print("\tend", cId)

    @ TimedFunction.time("Redundant.compute")
    def compute(self, antecedents, context):
        ineq = self.constraint
        witness = self.witness.get()
        negated = ineq.copy().negated()
        # Constraint is made available to be used in subproof for redundance-based strengthening
        # Constraint is not added to the database at this point.
        self.addAvailable(negated)

        self.proof = context.proof

        if self.proof:
            self.outNegRedId = context.proof.nextId()
            negated.setOutId(negated.minId, self.outNegRedId)
            ineq.setOutId(ineq.minId, self.outNegRedId)

        # set witness in subcontext to allow lazy creation of
        # proofgoals from the database
        self.subContext.witness = witness

        if self.autoProveAll:
            # rup check would be expensive if we only derive a new
            # reification, so only do rup check first if there are
            # many effected constraints, otherwise it will be cheap to
            # compute the effected constraints anyway.
            if getattr(context, "autoRUPstreak", 6) > 5:
                constraint_proven = False
                if ineq.rupCheck(context.propEngine, False, context.proof.getProofContext()):
                    constraint_proven = True
                    if self.proof:
                        context.proof.print("red", context.ineqFactory.toString(
                            self.constraint), ";", "; begin")
                        self.proof.print("\t* constraint is RUP")
                        self.proof.print("\tpol", self.proof.getBuffer())
                        self.proof.print("end", self.proof.nextId())
                        self.outRedId = self.proof.nextId()
                else:
                    found_constraint = context.propEngine.find(ineq, False)
                    if found_constraint is not None:
                        constraint_proven = True
                        if self.proof:
                            context.proof.print("red", context.ineqFactory.toString(
                                self.constraint), ";", "; begin")
                            self.proof.print("\tpol", self.outNegRedId, found_constraint.getOutId(
                                found_constraint.minId), "+")
                            self.proof.print("end", self.proof.nextId())
                            self.outRedId = self.proof.nextId()
                if constraint_proven:
                    assert (not self.subContext.subgoals)
                    # call auto proof with no subgoals to set correct state, but does not check anything
                    self.autoProof(context, antecedents,
                                   context.only_core_subproof)
                    # CLean up temporary constraint IDs
                    ineq.removeOutId(ineq.minId)
                    negated.removeOutId(negated.minId)
                    return super().compute(antecedents, context)

        if self.proof:
            context.proof.print("red", context.ineqFactory.toString(
                self.constraint), ";", self.witness.toString(context.ineqFactory), "; begin")

        if context.verifierSettings.trace:
            print("  ** proofgoals from formula **")

        effected = computeEffected(
            context, witness, context.only_core_subproof, context.strengthening_to_core)
        for ineq in effected:
            stats.numGoalCandidates += 1
            assert (ineq.minId != 0)
            self.makeGoal(ineq, negated, ineq.minId)

        if context.verifierSettings.trace:
            print("  ** proofgoal from satisfying added constraint **")
        ineq = self.constraint.copy()
        ineq.substitute(witness)
        self.makeGoal(ineq, negated)

        if context.verifierSettings.trace:
            print("  ** proofgoals from order **")
        orderContext = OrderContext.setup(context)
        order = orderContext.activeOrder

        witnessDict = self.witness.asDict()
        # Canonically include all the # proofgoals, which is not done if the next line is not commented out
        # if not order.varsSet.isdisjoint(witnessDict):
        orderConditions = order.getOrderCondition(self.witness.asDict())
        for ineq in orderConditions:
            self.makeGoal(ineq, negated)

        if context.verifierSettings.trace:
            print("  ** proofgoals from objective **")
        obj = objectiveCondition(context, self.witness.asDict())
        if obj is not None:
            self.makeGoal(obj, negated)

        if self.autoProveAll:
            self.autoProof(context, antecedents, context.only_core_subproof)

            # Check if autoproving was done for all subgoals combined (the subgoals were not needed for contradiction)
            if context.rupWithoutSubgoals:
                self.proof.print("end", self.proof.nextId())
            # Or only subgoal by subgoal
            else:
                self.proof.print("end")
            self.outRedId = context.proof.nextId()

        return super().compute(antecedents, context)

    def isAddToCore(self):
        # Add result of redundance-based strengthening to core set if `strengthening_to_core` is true and witness is non-empty.
        if self.autoProveAll and self.strengthening_to_core and not self.witness.isEmpty():
            return True
        else:
            # Set flag for subcontext to add result of redundance-based strengthening after subproof to core set.
            if self.strengthening_to_core and not self.witness.isEmpty():
                self.subContext.add_result_to_core = True
            return False

    def justify(self, antecedents, derived, context):
        assert (len(derived) <= 1)

        for constraint, idx in derived:
            if constraint:
                if self.autoProveAll:
                    constraint.setOutId(idx, self.outRedId)
                else:
                    constraint.setOutId(idx, self.outNegRedId)


@ register_rule
class DominanceRule(MultiGoalRule):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["dom"]

    @ classmethod
    def parse(cls, line, context):
        orderContext = OrderContext.setup(context)
        order = orderContext.activeOrder

        context.canLoadFormula = False

        with MaybeWordParser(line) as words:
            parser = OPBParser(
                ineqFactory=context.ineqFactory,
                allowEq=False)
            ineq = parser.parseConstraint(words)

            substitution = Substitution.parse(
                words=words,
                ineqFactory=context.ineqFactory)

            context.propEngine.increaseNumVarsTo(context.ineqFactory.numVars())

            autoProveAll = not cls.parseHasExplicitSubproof(words)

        return cls(context, ineq[0], substitution, order, autoProveAll)

    def __init__(self, context, constraint, witness, order, autoProveAll):
        super().__init__(context)

        self.constraint = constraint
        self.witness = witness
        self.order = order

        self.autoProveAll = autoProveAll
        self.strengthening_to_core = context.strengthening_to_core

        self.addIntroduced(constraint)

    def antecedentIDs(self):
        return "all"

    @ TimedFunction.time("DominanceRule.compute")
    def compute(self, antecedents, context):
        ineq = self.constraint.copy()
        negated = ineq.negated()
        self.addAvailable(negated)

        self.proof = context.proof

        if self.proof:
            self.outNegDomId = self.proof.nextId()
            negated.setOutId(negated.minId, self.outNegDomId)
            self.proof.print("dom", context.ineqFactory.toString(
                self.constraint), ";", self.witness.toString(context.ineqFactory), "; begin")

        self.proof.resetBuffer()
        effected = self.order.getCachedGoals(context, self.witness)
        # Write proof for directly proven goals when calculating the proofgoals
        self.proof.print(self.proof.getBuffer(), end='')

        for goal in effected:
            asRhs = goal.getAsRightHand()
            if asRhs is not None:
                assert (asRhs.minId != 0)
                if negated.implies(asRhs):
                    goal.isProven = True
                goalId = self.addSubgoal(goal, asRhs.minId)

                # Selflogging proof is goal is implied by negated constraint
                if self.proof and goal.isProven:
                    self.proof.print(
                        "\t* autoproven: goal is implied by negated constraint")
                    if isinstance(goalId, int):
                        self.proof.print(
                            "\tproofgoal", asRhs.getOutId(asRhs.minId))
                    else:
                        self.proof.print("\tproofgoal", goalId)
                    cId = self.proof.nextId()

                    self.proof.print("\t\tpol", cId, self.outNegDomId, "+")
                    cId = self.proof.nextId()
                    self.proof.print("\tend", cId)
            else:
                goalId = self.addSubgoal(goal)

        if self.autoProveAll:
            self.autoProof(context, antecedents, context.only_core_subproof)

            # Check if autoproving was done for all subgoals combined (the subgoals were not needed for contradiction)
            if context.rupWithoutSubgoals:
                self.proof.print("end", self.proof.nextId())
            # Or only subgoal by subgoal
            else:
                self.proof.print("end")
            self.outDomId = self.proof.nextId()

        return super().compute(antecedents, context)

    def isAddToCore(self):
        if self.autoProveAll and self.strengthening_to_core:
            return True
        else:
            if self.strengthening_to_core:
                self.subContext.add_result_to_core = True
            return False

    def justify(self, antecedents, derived, context):
        assert (len(derived) <= 1)

        for constraint, idx in derived:
            if constraint:
                if self.autoProveAll:
                    constraint.setOutId(idx, self.outDomId)
                else:
                    constraint.setOutId(idx, self.outNegDomId)


@register_rule
class StrengtheningToCore(EmptyRule):
    Ids = ["strengthening_to_core"]

    @classmethod
    def parse(cls, line, context):
        with MaybeWordParser(line) as words:
            cls.option = next(words, None)
            if cls.option is None or (cls.option != "on" and cls.option != "off"):
                raise InvalidProof(
                    "The rule 'strengthening_to_core' requires the option 'on' or 'off' (i.e. 'strengthening_to_core on|off').")

        return cls()

    def compute(self, antecedents, context):
        # Check if strengthening to core has been enable. If so, all derived constraints have to be moved to the set of core constraints.
        if self.option == "on":
            if context.strengthening_to_core:
                logging.warning(
                    "Proof enables strengthening to core mode, which was already enabled. Maybe this is not intentional and hints towards an error.")
            context.propEngine.moveAllToCore()
            context.strengthening_to_core = True
        else:
            if not context.strengthening_to_core:
                logging.warning(
                    "Proof disables strengthening to core mode, which was already disabled. Maybe this is not intentional and hints towards an error.")
            context.strengthening_to_core = False
        return []

    def justify(self, antecedents, derived, context):
        context.proof.print("strengthening_to_core", self.option)
