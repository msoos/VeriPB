from veripb.order import OrderContext
from veripb.substitution import Substitution
from veripb.rules_multigoal import *
from veripb.rules import idOrFind, ConstraintEquals
from veripb.rules_dominance import stats
from veripb.order import computeEffected, objectiveCondition
from veripb.timed_function import TimedFunction

from itertools import tee
import logging

# Despite the name unchecked deletion still has conditions to check for validity.


@register_rule
class DeleteConstraints2(MultiGoalRule):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["del"]

    @classmethod
    def parse(cls, words, context):
        toDelete, deletionType = idOrFind(words, context)

        substitution = Substitution.parse(
            words=words,
            ineqFactory=context.ineqFactory)

        autoProveAll = not cls.parseHasExplicitSubproof(words)

        return cls(context, toDelete, deletionType, substitution, autoProveAll)

    def __init__(self, context, toDelete, deletionType, substitution, autoProveAll):
        self.toDelete = toDelete
        self.deletionType = deletionType
        self.substitution = substitution
        self.autoProveAll = autoProveAll
        self.ineqFactory = context.ineqFactory
        self.displayGoals = context.verifierSettings.trace
        self.autoProoved = True
        self.constraints = []

    # Unchecked deletions from the core are only allowed when ((the strengthening to core is off AND no order is loaded) OR all constraints are currently in the core). All constraints will be moved to the core if needed to satisfy second condition.
    def checkUncheckedDeletion(self, context):
        # check if order is trivial
        orderContext = OrderContext.setup(context)
        order = orderContext.activeOrder
        if not order.name == "":
            # Order is loaded and all derived constraints are moved to core set
            context.propEngine.moveAllToCore()
        else:
            # No order is loaded
            if context.strengthening_to_core:
                # strengthening to core is on
                context.propEngine.moveAllToCore()

    def makeGoal(self, ineq, negated, goalId=None):
        goal = SubGoal(ineq)
        if negated.implies(ineq):
            goal.isProven = True
        else:
            stats.numSubgoals += 1
        goalId = self.addSubgoal(goal, goalId)

        if self.proof and goal.isProven:
            self.proof.print(
                "\t* autoproven: goal is implied by negated constraint")
            if isinstance(goalId, int):
                self.proof.print("\tproofgoal", ineq.getOutId(goalId))
            else:
                self.proof.print("\tproofgoal", goalId)
            cId = self.proof.nextId()

            self.proof.print("\t\tpol", cId, self.outNegRedId, "+")
            cId = self.proof.nextId()
            self.proof.print("\tend", cId)

    @TimedFunction.time("CheckedDeletionCheck")
    def checkCheckedDeletion(self, context, antecedents, constraint, constraint_id):
        # Initialize checking context.
        self.subContexts = SubContext.setup(context)
        self.subContext = self.subContexts.push()
        self.constraints = []
        self.nextId = 1
        self.autoProoved = False
        self.proof = context.proof
        witness = self.substitution.get()
        negated = constraint.copy().negated()

        # Move constraint to derived set, so that all the checks using this constraint will fail.
        context.propEngine.moveToDerived(constraint, constraint_id)

        # Constraint is made available to be used in subproof for redundance-based strengthening
        # Constraint is not added to the database at this point.
        self.addAvailable(negated)

        # set witness in subcontext to allow lazy creation of
        # proofgoals from the database
        self.subContext.witness = witness

        if self.autoProveAll:
            # rup check would be expensive if we only derive a new
            # reification, so only do rup check first if there are
            # many effected constraints, otherwise it will be cheap to
            # compute the effected constraints anyway.
            if getattr(context, "autoRUPstreak", 6) > 5:
                constraint_proven = False
                if constraint.rupCheck(context.propEngine, True, context.proof.getProofContext()):
                    constraint_proven = True
                    if self.proof:

                        context.proof.print("delc", constraint.getOutId(constraint_id), ";", self.substitution.toString(
                            context.ineqFactory), "; begin")
                        # Skip ID for the negated constraint
                        self.proof.nextId()
                        self.proof.print("\t* constraint is RUP")
                        self.proof.print("\tpol", self.proof.getBuffer())
                        self.proof.print("end", self.proof.nextId())
                else:
                    found_constraint = context.propEngine.find(
                        constraint, True)
                    if found_constraint is not None:
                        constraint_proven = True
                        if self.proof:
                            context.proof.print("delc", constraint.getOutId(
                                constraint_id), ";", self.substitution.toString(context.ineqFactory), "; begin")
                            self.proof.print("\t* constraint is in DB")
                            self.proof.print("\tpol", self.proof.nextId(), found_constraint.getOutId(
                                found_constraint.getSomeCoreId()), "+")
                            self.proof.print("end", self.proof.nextId())
                if constraint_proven:
                    assert (not self.subContext.subgoals)
                    # call auto proof with no subgoals to set correct state, but does not check anything
                    self.autoProof(context, antecedents)
                    # CLean up temporary constraint IDs
                    constraint.removeOutId(constraint.minId)
                    negated.removeOutId(negated.minId)
                    return
        else:  # There is an explicit subproof
            if context.only_core_subproof:
                raise NotImplementedError(
                    "Nesting of only core subproofs has not been implemented, yet. Only core subproofs are required for checked deletion and objective update.")
            context.only_core_subproof = True
            self.subContext.set_only_core_subproof = True

        if self.proof:
            context.proof.print("delc", constraint.getOutId(
                constraint_id), ";", self.substitution.toString(context.ineqFactory), "; begin")
            # Get ID for negated constraint
            self.outNegRedId = context.proof.nextId()
            negated.setOutId(negated.minId, self.outNegRedId)

        if context.verifierSettings.trace:
            print("  ** proofgoals from formula **")

        effected = computeEffected(
            context, witness, context.only_core_subproof)
        for ineq in effected:
            stats.numGoalCandidates += 1
            assert (ineq.minId != 0)
            self.makeGoal(ineq, negated, ineq.minId)

        if context.verifierSettings.trace:
            print("  ** proofgoal from satisfying added constraint **")
        ineq = constraint.copy()
        ineq.substitute(witness)
        self.makeGoal(ineq, negated)

        if context.verifierSettings.trace:
            print("  ** proofgoals from order **")
        orderContext = OrderContext.setup(context)
        order = orderContext.activeOrder

        # Canonically include all the # proofgoals, which is not done if the next line is not commented out
        # if not order.varsSet.isdisjoint(witnessDict):
        orderConditions = order.getOrderCondition(self.substitution.asDict())
        for ineq in orderConditions:
            self.makeGoal(ineq, negated)

        if context.verifierSettings.trace:
            print("  ** proofgoals from objective **")
        obj = objectiveCondition(context, self.substitution.asDict())
        if obj is not None:
            self.makeGoal(obj, negated)

        if self.autoProveAll:
            self.autoProof(context, antecedents, True)

            # Check if autoproving was done for all subgoals combined (the subgoals were not needed for contradiction)
            if context.rupWithoutSubgoals:
                self.proof.print("end", self.proof.nextId())
            # Or only subgoal by subgoal
            else:
                self.proof.print("end")

    def compute(self, antecedents, context):
        actualDeletion = set()
        ineqid_to_ineq_deletions = dict()
        for antecedent in antecedents:
            if antecedent is None:
                break
            ineq, ineqid = antecedent
            deletions = context.propEngine.getDeletions(ineq)
            actualDeletion.update(deletions)
            if len(deletions) > 0:
                ineqid_to_ineq_deletions[ineqid] = ineq
            if not deletions and self.deletionType != "find":
                actualDeletion.add(ineqid)
                ineqid_to_ineq_deletions[ineqid] = ineq

        checked_core_deletion_counter = 0
        for ineqid, ineq in ineqid_to_ineq_deletions.items():
            # deletion of this constraint needs to be checked.
            if ineq.isCoreId(ineqid):
                # Checked deletion
                if context.deletion_checked:
                    # We need to do some checks that only have to be performed once when we delete the first core constraint and the second core constraint.
                    if checked_core_deletion_counter == 0:
                        # If strengthening to core is enabled, then checked deletion cannot use a witness.
                        if context.strengthening_to_core and not self.substitution.isEmpty():
                            raise InvalidProof(
                                "Checked deletion with a witness cannot be used while strengthening to core is activated!")
                    if checked_core_deletion_counter == 1:
                        # If we want to delete more than one core constraint, then witness has to be empty.
                        if not self.substitution.isEmpty():
                            raise InvalidProof(
                                "Cannot do checked deletion of more than one constraint using a witness, as this is undefined behaviour. Only one core deletion allowed per deletion rule when witness is used!")
                        if not self.autoProveAll:
                            raise InvalidProof(
                                "Subproof is not allowed for deleting multiple core constraints. The deletion step should be split up into multiple deletion steps with individual subproofs.")

                    # Try checked deletion
                    # Set up selflogging to undo proof attempt for checked deletion
                    context.proof.setTemporaryProofBuffer()
                    id_before_check = context.proof.lastId()
                    try:
                        self.checkCheckedDeletion(
                            context, antecedents, ineq, ineqid)
                        context.proof.writeTemporaryProofBuffer()
                    except InvalidProof as e:
                        # Deletion check has failed.
                        if context.force_checked_deletion:
                            raise InvalidProof(
                                "Checked deletion failed: " + str(e))
                        else:
                            context.deletion_checked = False
                            logging.warning(
                                "Changing from checked deletion checks to unchecked deletion checks, since deletion check failed. Use the option '--forceCheckDeletion' to force the proof checking to fail if deletion check fails.")
                            # Undo proof for checked deletion proof attempt.
                            context.propEngine.moveToCore(ineq, ineqid)
                            context.proof.forgetTemporaryProofBuffer()
                            context.proof.setId(id_before_check)
                            self.constraints = list()
                            self.subContexts.pop()
                            self.autoProoved = True
                    checked_core_deletion_counter += 1

                # Unchecked deletion
                # This is not an else-branch, as unchecked deletion still has to be performed after failed checked deletion.
                if not context.deletion_checked:
                    self.checkUncheckedDeletion(context)

        self.toDelete = list(actualDeletion)
        return super().compute(antecedents, context)

    def antecedentIDs(self):
        return ("all_plus", self.toDelete)

    def deleteConstraints(self):
        if self.autoProveAll:
            return self.toDelete
        else:
            self.subContext.addToDelete(self.toDelete)
            return []

    def isDeleteRule(self):
        return True

    def justify(self, antecedents, derived, context):
        pass


@register_rule
class DeleteConstraintsCore(DeleteConstraints2):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["delc"]

    @classmethod
    def parse(cls, line, context):
        if context.major == 1:
            raise InvalidProof(
                "The rule 'delc' is not available in pseudo-Boolean proof version 1.x.")
        which = []
        with MaybeWordParser(line) as words:
            while True:
                try:
                    token = next(words)
                    if token == ";":
                        break
                    else:
                        which.append(int(token))
                except:
                    break

            if (which[-1] == 0):
                which = which[:-1]

            if 0 in which:
                raise InvalidProof("Can not delete constraint with index 0.")

            substitution = Substitution.parse(
                words=words,
                ineqFactory=context.ineqFactory)

            autoProveAll = not cls.parseHasExplicitSubproof(words)

        return cls(context, which, "id", substitution, autoProveAll)

    def compute(self, antecedents, context):
        antecedents, antecedent_local = tee(antecedents)
        for antecedent in antecedent_local:
            if antecedent is None:
                break
            ineq, ineqid = antecedent
            if not ineq.isCoreId(ineqid):
                raise InvalidProof(
                    "Constraint with ID %s to be deleted by 'delc' is not a core constraint ID." % str(ineqid))
        return super().compute(antecedents, context)


# Proof logging for deleting derived constraints in done in Verifier
@register_rule
class DeleteConstraintsDerived(EmptyRule):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["deld"]

    @classmethod
    def parse(cls, line, context):
        if context.major == 1:
            raise InvalidProof(
                "The rule 'deld' is not available in pseudo-Boolean proof version 1.x.")
        with MaybeWordParser(line) as words:
            which = list(map(int, words))

            if (which[-1] == 0):
                which = which[:-1]

            if 0 in which:
                raise InvalidProof("Can not delete constraint with index 0.")

        return cls(which)

    def __init__(self, toDelete):
        self.toDelete = toDelete

    def compute(self, antecedents, context):
        for ineq, ineqid in antecedents:
            if not ineq.isDerivedId(ineqid):
                raise InvalidProof(
                    "Constraint with ID %s to be deleted by %s is not a derived constraint ID." % (str(ineqid),

                                                                                                   str(self.Ids[0])))
        return []

    def isDeleteRule(self):
        return True

    def deleteConstraints(self):
        return self.toDelete

    def antecedentIDs(self):
        return self.toDelete


@register_rule
class DeleteConstraints(DeleteConstraints2):
    subRules = MultiGoalRule.subRules + \
        [ReversePolishNotation, IsContradiction, ConstraintEquals]
    Ids = ["d"]  # (d)elete

    @classmethod
    def parse(cls, line, context):
        which = []
        with MaybeWordParser(line) as words:
            while True:
                try:
                    token = next(words)
                    if token == ";":
                        break
                    else:
                        which.append(int(token))
                except:
                    break

            if (which[-1] == 0):
                which = which[:-1]

            if 0 in which:
                raise InvalidProof("Can not delete constraint with index 0.")

            substitution = Substitution.parse(
                words=words,
                ineqFactory=context.ineqFactory)

            autoProveAll = not cls.parseHasExplicitSubproof(words)

        return cls(context, which, "id", substitution, autoProveAll)


class LevelStack():
    @classmethod
    def setup(cls, context, namespace=None):

        try:
            return context.levelStack[namespace]
        except AttributeError:
            context.levelStack = dict()
            addIndex = True
        except IndexError:
            addIndex = True

        if addIndex:
            levelStack = cls()
            context.levelStack[namespace] = levelStack
            def f(ineqs, context): return levelStack.addToCurrentLevel(ineqs)
            context.addIneqListener.append(f)
            return context.levelStack[namespace]

    def __init__(self):
        self.currentLevel = 0
        self.levels = list()

    def setLevel(self, level):
        self.currentLevel = level
        while len(self.levels) <= level:
            self.levels.append(list())

    def addToCurrentLevel(self, ineqs):
        self.levels[self.currentLevel].extend(ineqs)

    def wipeLevel(self, level):
        if level >= len(self.levels):
            raise ValueError(
                "Tried to wipe level %i that was never set." % (level))

        result = list()
        for i in range(level, len(self.levels)):
            result.extend(self.levels[i])
            self.levels[i].clear()

        return result


@register_rule
class SetLevel(EmptyRule):
    Ids = ["#"]

    @classmethod
    def parse(cls, line, context):
        levelStack = LevelStack.setup(context)
        with MaybeWordParser(line) as words:
            level = words.nextInt()
            words.expectEnd()
        levelStack.setLevel(level)

        return cls()


@register_rule
class WipeLevel(DeleteConstraints2):
    Ids = ["w"]

    @classmethod
    def parse(cls, line, context):
        levelStack = LevelStack.setup(context)
        with MaybeWordParser(line) as words:
            level = words.nextInt()
            try:
                words.expectEnd()
            except ValueError:
                raise InvalidProof(
                    "Expected end of line. The wipe-out rule has the syntax 'w <level>' and cannot be used together with a substitution.")

        return cls(context, levelStack.wipeLevel(level), "id", Substitution(), True)
